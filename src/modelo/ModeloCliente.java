package modelo;


public class ModeloCliente {
    
    private String cpf;
    private String nome;
    private String sexo;
    private String endereco;
    

    public String getCpf() {
        return this.cpf;
    }

    public void setCpf(String cpf) { 
        this.cpf = cpf;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSexo() {
        return this.sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEndereco() {
        return this.endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }       
        
    public String imprime(){
        return "Oi, sou um cliente";
    }
}

    

