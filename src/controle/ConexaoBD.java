
package controle;


//Importações
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

//JOPTION PANE pra exibir diálogo ou mensagem
import javax.swing.JOptionPane;

//Método Construtor
public class ConexaoBD {
    
    public Statement stm; //responsável por buscar em BD
    public ResultSet ra;  //armazena o resultado dessa pesquisa
    private static String driver = "com.mysql.jdbc.Driver"; //identifica o serviço no BD
    private static String caminho = "jdbc:mysql://localhost:3306/bibliotec"; //Diz qual o caminho do BD
    private static String usuario = "root"; //Diz qual o LOGIN do usuário
    private static String senha = "serra"; //E sua senha
    public Connection con; //realiza a conexão com o BD

    //SOBRECARGA
    public static Connection getConnection(){
       
        Connection con = null; //adc
        try {
            Class.forName(driver);
            
            return DriverManager.getConnection(caminho, usuario, senha);
            
        } catch (ClassNotFoundException | SQLException ex) {
            throw new RuntimeException("erro na conexão", ex);
        }
    
    }
    
    
    public static void closeConnection(Connection con){       
        if(con != null){           
            try {
                con.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao fechar conexão: "+ex);
            }            
        }  
    }
    
    public static void closeConnection(Connection con, PreparedStatement stmt, ResultSet rs){
       
        if(rs != null){          
            try {
                rs.close();
            } catch (SQLException ex) {
                System.err.println("Erro: "+ex);            
            } 
          
        } 
        closeConnection(con);
    }
    
      public void conexao(){
        
        try {
            System.setProperty("jdbc.Drivers", driver);
            con=DriverManager.getConnection(caminho, usuario, senha);
            JOptionPane.showMessageDialog(null, "Conexão efetuada com sucesso!");
         } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao se conectar com o banco de dados!\n"+ex);
        }
    }

    public void desconecta() {
        
    }
    

   
}


