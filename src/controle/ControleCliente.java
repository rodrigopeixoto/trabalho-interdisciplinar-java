/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;


//Fazemos importações para funcionamento da conexão
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import modelo.ModeloCliente;


public class ControleCliente {
    
    ConexaoBD conex = new ConexaoBD(); //Novo objeto de referência à classe de Conexão com o Banco
    modelo.ModeloCliente mod = new ModeloCliente(); //E outro com a Classe ModeloCliente, onde estão armazenados os dados
    
    public void botaoSalvar(ModeloCliente mod){ //O objeto Modelo cliente é passado como parâmetro, pois precisaremos de suas informações  
        conex.conexao(); //uma vez conectado com o banco
        try {
            
            //Na linha abaixo inserimos os dados do cliente dentro das respectivas colunas do Banco de valor
            PreparedStatement pst = conex.con.prepareStatement("insert into pessoa(nome, cpf, sexo, endereco) values(?,?,?,?)");            
            
            //Pegamos os os dados e inserimos NAS RESPECTIVAS POSIÇÕES
            pst.setString(1, mod.getNome()); //posição 1
            pst.setString(2, mod.getCpf()); //posição 2
            pst.setString(3, mod.getSexo()); //posição 3
            pst.setString(4, mod.getEndereco()); //posição 4
            pst.execute(); //Executa
            JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso!"); //Mostra que os dados foram inseridos
            
        } catch (SQLException ex) { //Senão
            
            JOptionPane.showMessageDialog(null, "Erro ao inserir os dados! /nErro: "+ex); //Exibe a mensagem de erro (e o diagnóstico!)
        }
        
        conex.desconecta(); //Desconecta
    
    }
    
}
