
package visão;

//
import controle.ConexaoBD; //importamos a classe de Conexão com  o Banco, que está no pacote de controle 
import java.util.Vector;  //E um vetor

//Como a classe principal é uma Tela (um Jframe) então temos que fazer a herança  
public class Principal extends javax.swing.JFrame { 

    ConexaoBD conecta = new ConexaoBD(); //Uma referência à Classe que faz e testa a conexão com o Banco de Dados 

    public Principal() { //Método Construtor, o único método que pode começar com letra maíuscula
        initComponents();
        conecta.conexao(); 
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        fundo = new javax.swing.JLabel();
        jInternalFrame1 = new javax.swing.JInternalFrame();
        painel = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        botaoCliente = new javax.swing.JButton();
        botaoCalculaMulta = new javax.swing.JButton();
        botaoSair = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        fundo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/esse.jpg"))); // NOI18N

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        jInternalFrame1.setTitle("Bem-Vindo");
        jInternalFrame1.setVisible(true);
        jInternalFrame1.getContentPane().setLayout(null);

        painel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        painel.setLayout(null);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Cadastro");
        painel.add(jLabel3);
        jLabel3.setBounds(20, 40, 70, 20);

        botaoCliente.setText("Cliente");
        botaoCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoClienteActionPerformed(evt);
            }
        });
        painel.add(botaoCliente);
        botaoCliente.setBounds(10, 70, 80, 50);

        botaoCalculaMulta.setText("Calcular multa");
        botaoCalculaMulta.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                botaoCalculaMultaMouseClicked(evt);
            }
        });
        botaoCalculaMulta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoCalculaMultaActionPerformed(evt);
            }
        });
        painel.add(botaoCalculaMulta);
        botaoCalculaMulta.setBounds(110, 70, 130, 50);

        botaoSair.setText("Sair");
        botaoSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoSairActionPerformed(evt);
            }
        });
        painel.add(botaoSair);
        botaoSair.setBounds(193, 3, 50, 20);

        jInternalFrame1.getContentPane().add(painel);
        painel.setBounds(10, 20, 250, 130);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Sistema de gerenciamento de:");
        jInternalFrame1.getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 0, 220, 15);

        getContentPane().add(jInternalFrame1);
        jInternalFrame1.setBounds(0, 10, 280, 190);

        setSize(new java.awt.Dimension(300, 242));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void botaoClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoClienteActionPerformed
        FormCliente tela = new FormCliente(); //Criamos um novo JForm do tipo FormCliente() chamado tela
        tela.setVisible(true); //e tornamos essa tela visível aqui
    }//GEN-LAST:event_botaoClienteActionPerformed

    private void botaoSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoSairActionPerformed
        System.exit(0); //Para fechar o programa ao clicar no botão Sair
    }//GEN-LAST:event_botaoSairActionPerformed

    private void botaoCalculaMultaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoCalculaMultaActionPerformed
       CalcularMulta tela = new CalcularMulta(); //Criamos um novo JForm do tipo CalcularMulta() chamado tela
       tela.setVisible(true); //e tornamos essa tela visível aqui       
    }//GEN-LAST:event_botaoCalculaMultaActionPerformed

    private void botaoCalculaMultaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botaoCalculaMultaMouseClicked
          
    }//GEN-LAST:event_botaoCalculaMultaMouseClicked


    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new Principal().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botaoCalculaMulta;
    private javax.swing.JButton botaoCliente;
    private javax.swing.JButton botaoSair;
    private javax.swing.JLabel fundo;
    private javax.swing.JInternalFrame jInternalFrame1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel painel;
    // End of variables declaration//GEN-END:variables
}
