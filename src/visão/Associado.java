
package visão;

public class Associado extends Pessoa { //HERANÇA

    public Associado (String cpf, String nome, String sexo, String endereco){
        super(cpf, nome, sexo, endereco); //Tem que estar na mesma ordem do Construtor
    }
  
    public String imprime(){ //polimorfismo, pois o método tem o mesmo nome

    return "Oi, eu sou um associado";
    }  
}
