
package visão;

//Importações
import java.util.Date; //Data 
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class CalcularMulta extends javax.swing.JFrame {

    private Principal principal; //Referência à classe principal, que tem que estar no Construtor
            
    public CalcularMulta() {
        initComponents();
        this.principal = principal; //
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        campoDataPrevista = new javax.swing.JTextField();
        campoDataAtual = new javax.swing.JTextField();
        botaoCalcularMulta = new javax.swing.JButton();
        campoValor = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        botaoSair2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(null);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Data Prevista:");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(10, 50, 90, 17);

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Data Atual:");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(10, 100, 80, 17);

        campoDataPrevista.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                campoDataPrevistaActionPerformed(evt);
            }
        });
        jPanel1.add(campoDataPrevista);
        campoDataPrevista.setBounds(110, 50, 210, 30);

        campoDataAtual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                campoDataAtualActionPerformed(evt);
            }
        });
        jPanel1.add(campoDataAtual);
        campoDataAtual.setBounds(110, 100, 210, 30);

        botaoCalcularMulta.setText("Calcular");
        botaoCalcularMulta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoCalcularMultaActionPerformed(evt);
            }
        });
        jPanel1.add(botaoCalcularMulta);
        botaoCalcularMulta.setBounds(10, 150, 80, 40);

        campoValor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                campoValorActionPerformed(evt);
            }
        });
        jPanel1.add(campoValor);
        campoValor.setBounds(160, 160, 160, 30);

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel4.setText("Valor:");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(110, 160, 50, 30);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("Calcular Multa");

        botaoSair2.setText("sair");
        botaoSair2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoSair2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(botaoSair2)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(121, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(botaoSair2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
        
    private void campoDataPrevistaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_campoDataPrevistaActionPerformed
        
    }//GEN-LAST:event_campoDataPrevistaActionPerformed
        
    private void campoDataAtualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_campoDataAtualActionPerformed
        
    }//GEN-LAST:event_campoDataAtualActionPerformed
        
    private void botaoCalcularMultaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoCalcularMultaActionPerformed
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); //Especificamos uma data com um tipo de data
        
        try {
            
            Date DataPre = (Date) sdf.parse(campoDataPrevista.getText()); //Pega o conteúdo do campo de data prevista e armazenada
            Date DataAtual = (Date) sdf.parse(campoDataAtual.getText()); //O mesmo é feito com a data atual 
            long diffEmMil = Math.abs(DataPre.getTime() - DataAtual.getTime()); //A diferença entre as datas
            campoValor.setText("R$"+ diffEmMil /(1000 * 60 * 60 * 24 +1)*1.75); //Fazemos uma conversão depois multiplica pelo valor da multa
            
        } catch (ParseException ex) {
            
            JOptionPane.showMessageDialog(null, "Modelo de data inválido, use dd/MM/yyyy "); //caso o modelo de data inserido seja inválido            
            //Logger.getLogger(CalcularMulta.class.getName()).log(Level.SEVERE, null, ex);  
        }
                                            
    }//GEN-LAST:event_botaoCalcularMultaActionPerformed

    private void campoValorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_campoValorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_campoValorActionPerformed

    private void botaoSair2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoSair2ActionPerformed
       System.exit(0);
    }//GEN-LAST:event_botaoSair2ActionPerformed

    /**
     * @param args the command line arguments
     */


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botaoCalcularMulta;
    private javax.swing.JButton botaoSair2;
    private javax.swing.JTextField campoDataAtual;
    private javax.swing.JTextField campoDataPrevista;
    private javax.swing.JTextField campoValor;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
